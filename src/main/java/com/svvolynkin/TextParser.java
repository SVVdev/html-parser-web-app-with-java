package com.svvolynkin;

import org.kefirsf.bb.BBProcessorFactory;
import org.kefirsf.bb.TextProcessor;

/**
 * <h1>Text parser</h1>
 * The utility class that can be used to parse texts
 *
 * @author  Sergey Volynkin
 * @author  SVVolynkin@gmail.com
 * @version 0.1
 * @since   2017-10-19
 */
public final class TextParser {

    /**
     * Private constructor to prevent instantiation of the class.
     */
    private TextParser() {
    }


    /**
     * Method to transform BBcode formatted text to HTML
     * @param s A string to transform to HTML
     * @return HTML formatted text
     */
    public static String parseToHTML(String s){

        TextProcessor textProcessor = BBProcessorFactory.getInstance().create();
        return textProcessor.process(s);

    }

}
