package com.svvolynkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>BBcode to HTML parser</h1>
 * The program implements an web application that
 * allows convert bbcode formatted text to HTML
 * and display html formatted text on the client-side
 * <b>Note:</b> it's a Spring Boot application
 *
 * @author  Sergey Volynkin
 * @author  SVVolynkin@gmail.com
 * @version 1.0
 * @since   2017-10-19
 */
@SpringBootApplication
public class Application {

    /**
     * This is the main method which starts up {@link SpringApplication}.
     * @param args Unused.
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
