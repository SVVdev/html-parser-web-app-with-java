package com.svvolynkin;

/** Represents an BBcode formatted text.
 * @author Sergey Volynkin
 * @author SVVolynkin@gmail.com
 * @version 1.0
 * @since 2017-10-19
 */
public class TextMessage {

    private String content;

    /** Gets the text’s content.
     * @return content of the text
     */
    public String getContent() {
        return content;
    }

    /** Sets the text’s content.
     * @param content A String containing BBcode formatted text.
     */
    public void setContent(String content) {
        this.content = content;
    }
}
