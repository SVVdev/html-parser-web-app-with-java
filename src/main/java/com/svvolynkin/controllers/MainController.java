package com.svvolynkin.controllers;

import com.svvolynkin.TextMessage;
import com.svvolynkin.TextParser;
import org.slf4j.Logger; // Logback logger is default for Spring Boot
import org.slf4j.LoggerFactory; // Logback logger is default for Spring Boot
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * <h1>Main Controller</h1>
 * Processes GET and POST requests from the client
 *
 * @author  Sergey Volynkin
 * @author  SVVolynkin@gmail.com
 * @version 1.0
 * @since   2017-10-19
 */

@Controller
public class MainController {

    /**
     * Logger Logback (is default for Spring Boot) for logging
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Returns html page to render when client
     * trying to reach "/parse" directory of the web application
     * with the GET request
     * @param model A Model stores data for rendering the html page
     * @return string name of the html page to render
     */
    @GetMapping("/parse")
    public String parseForm(Model model) {
        model.addAttribute("parse", new TextMessage());
        logger.info("Opening main.html");
        return "main";
    }

    /**
     * Returns html page to render when client
     * trying to reach "/parse" directory of the web application
     * with the POST request
     * @param textMessage A TextMessage object that stores string that
     *                    containing BBcode formatted text.
     * @return string name of the html page to render
     */
    @PostMapping("/parse")
    public String parseSubmit(@ModelAttribute TextMessage textMessage) {
        String stringToParse = textMessage.getContent();
        logger.info("Text for parsing was received");
        textMessage.setContent(TextParser.parseToHTML(stringToParse));
        logger.info("Text has been parsed and written back to the TextMessage object");
        logger.info("Opening result.html");
        return "result";
    }

}
