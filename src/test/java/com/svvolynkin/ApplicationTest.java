package com.svvolynkin;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * <h1>Application tests</h1>
 * Tests for the features of the application
 *
 * @author  Sergey Volynkin
 * @author  SVVolynkin@gmail.com
 * @version 0.1
 * @since   2017-10-19
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = "logging.level.org.springframework.web=DEBUG")
@AutoConfigureMockMvc
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Open parser
     * Expecting form block will be rendered
     * @throws Exception ex
     */
    @Test
    public void rendersForm() throws Exception {
        mockMvc.perform(get("/parse"))
                .andExpect(content().string(containsString("Form")));
    }

    /**
     * Parse BBcode tag [b]message[/b]
     * Expecting tag converted to <b>message</b>
     * @throws Exception ex
     */
    @Test
    public void submitsFormTagB() throws Exception {
        mockMvc.perform(post("/parse").param("content", "[b]ICL[/b]"))
                .andExpect(content().string(containsString("<b>ICL</b>")));
    }

    /**
     * Parse BBcode tag [i]message[/i]
     * Expecting tag converted to <i>message</i>
     * @throws Exception ex
     */
    @Test
    public void submitsFormTagI() throws Exception {
        mockMvc.perform(post("/parse").param("content", "[i]GDC[/i]"))
                .andExpect(content().string(containsString("<i>GDC</i>")));
    }

    /**
     * Parse BBcode tag [url=http://site.com]Site[/url]
     * Expecting result tag converted to <a href="http://site.com">Site</a>
     * @throws Exception ex
     */
    @Test
    public void submitsFormTagURL() throws Exception {
        mockMvc.perform(post("/parse").param("content", "[url=http://icl-services.com]ICL Services[/url]"))
                .andExpect(content().string(containsString("<a href=\"http://icl-services.com\">ICL Services</a>")));
    }

}
